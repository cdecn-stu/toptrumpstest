﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player
{
    private Queue<Character> cards;
    public int playerID { get; }
    public GameObject cardModel { get; }

    public Character currentCard;
    public bool currentPicker;
    public int numberOfCardsLeft;

    public Player(int IDofPlayer)
    {

        cards = new Queue<Character>();
        playerID = IDofPlayer;
        cardModel = UnityEngine.MonoBehaviour.Instantiate(Resources.Load<GameObject>("cardModel"));
        cardModel.transform.position = new Vector3((cardModel.transform.localScale.x + 2) * playerID, 3.5f, 0);
        currentPicker = false;
        numberOfCardsLeft = cards.Count;
    } 

    public void AddCard(Character card)
    {
        cards.Enqueue(card);
        currentCard = cards.Peek();
    }

    public void AddCards(Character[] cardsToAdd)
    {
        foreach (Character card in cardsToAdd)
        {
            cards.Enqueue(card);
        }
    }

    public void DisplayCard()
    {
        Transform ComponentToChange = cardModel.transform.GetChild(0).GetChild(0);
        cardModel.transform.GetChild(0).rotation = new Quaternion(0, 1, 0, 0);

        ComponentToChange.GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>(currentCard.ImageName);
        ComponentToChange.GetChild(1).GetComponent<Text>().text = currentCard.CharacterName;
        ComponentToChange.GetChild(2).GetComponent<Text>().text = "Games: " + currentCard.NumberOfGames;
        ComponentToChange.GetChild(3).GetComponent<Text>().text = "Spin-Off Series: " + currentCard.NumberOfSpinOffSeries;
        ComponentToChange.GetChild(4).GetComponent<Text>().text = "First Appearance: " + currentCard.FirstAppearance;
        ComponentToChange.GetChild(5).GetComponent<Text>().text = "Latest Appearance:" + currentCard.LatestAppearance;

        /*
        GameObject.Find("CharacterName").GetComponent<Text>().text = characters[currentCardCount].CharacterName;
		GameObject.Find("CharacterImage").GetComponent<Image>().sprite = Resources.Load<Sprite>(characters[currentCardCount].ImageName);

		GameObject.Find("NumberOfGames").GetComponent<Text>().text = "Games: "+ characters[currentCardCount].NumberOfGames;
		GameObject.Find("NumberOfSpinOffs").GetComponent<Text>().text = "Spin-Off Series: " + characters[currentCardCount].NumberOfSpinOffSeries;
		GameObject.Find("FirstAppearance").GetComponent<Text>().text = "First Appearance: " + characters[currentCardCount].FirstAppearance;
		GameObject.Find("LatestAppearance").GetComponent<Text>().text = "Latest Appearance:"+ characters[currentCardCount].LatestAppearance;
        */
    }

    public void HideCard()
    {
        cardModel.transform.GetChild(0).rotation = new Quaternion(1, 0, 0, 0);
    }
    
    public void SetCurrentCard(bool requeueCard)
    {
        Character temp = currentCard;
        cards.Dequeue();

        if (requeueCard == true)
        {
            cards.Enqueue(temp);
        }

        currentCard = cards.Peek();
    }

    public void UpdateCardsLeft()
    {
        numberOfCardsLeft = cards.Count;
    }
}

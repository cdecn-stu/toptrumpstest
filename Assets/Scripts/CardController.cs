﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class CardController :MonoBehaviour , IPointerClickHandler
{
    void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
    {
        Debug.Log("Clicked: " + eventData.pointerCurrentRaycast.gameObject.name);

        //use a switch/case based on gameObject name to work decide which category has been clicked and then call the method in GameController
        switch (eventData.pointerCurrentRaycast.gameObject.name)
        {
            case "NumberOfGames":
                GameController.Instance.EvaluateCardFromPlayer(GameController.Categories.GAMES);
                break;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuScript : MonoBehaviour
{
    public Slider slider;
    public Text sliderText;

    public void SliderValueChanged()
    {
        GameController.numberOfPlayers = (int)slider.value;
        sliderText.text = "Players: " + GameController.numberOfPlayers.ToString();
    }

    public void SceneToLoad(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }
}

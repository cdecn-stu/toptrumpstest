﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character 
{
    private string characterName;
    private string imageName;
    private int numberOfGames;
    private int numberOfSpinOffSeries;
    private string firstAppearance;
    private string latestAppearance;

    public string CharacterName { get => characterName; }
    public string ImageName { get => imageName; }
    public int NumberOfGames { get => numberOfGames; }
    public int NumberOfSpinOffSeries { get => numberOfSpinOffSeries; }
    public string FirstAppearance { get => firstAppearance; }
    public string LatestAppearance { get => latestAppearance; }

    public Character(string characterName, string imageName, int numberOfGames, int numberOfSpinOffSeries, string firstAppearance, string latestAppearance)
    {
        this.characterName = characterName;
        this.imageName = imageName;
        this.numberOfGames = numberOfGames;
        this.numberOfSpinOffSeries = numberOfSpinOffSeries;
        this.firstAppearance = firstAppearance;
        this.latestAppearance = latestAppearance;
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : Singleton<GameController>
{
    public enum Categories
    {
        GAMES,
        SPINOFFS,
        FIRST,
        LATEST
    }

    private Character[] characters;

    /*
    [SerializeField]
	private int currentCardCount = 0;
    */

    [Range(2, 6)]
    public static int numberOfPlayers = 2;
    public bool pauseGameInput = false;

    private Queue<Player> players;

    // Start is called before the first frame update

    void Start()
    {
        characters = MyUtilities.LoadXMLData("characters");

        CreatePlayers();
        DealCards();
        StartNewTurn();
    }

    private void AddCardsToWinner(Character[] cardsToAdd, Player player)
    {
        player.AddCards(cardsToAdd);
    }

    private void ChooseWinningCard(Character winningCard, Character[] cardsToAdd)
    {
        Player winningPlayer;

        winningPlayer = FindOwnerOfWinningCard(winningCard); //fix for multiple cards and draw

        foreach (Player player in players)
        {
            player.currentPicker = false;
        }
        Debug.Log("fnkjelrw1");

        winningPlayer.currentPicker = true;
        Debug.Log("fnkjelrw2");

        AddCardsToWinner(cardsToAdd, winningPlayer);
        Debug.Log("fnkjelrw3");
        ShowAllCurrentCards();

        pauseGameInput = true;
        Debug.Log("fnkjelrw4");
        StartCoroutine("WaitForInput", winningPlayer);
        Debug.Log("fnkjelrw5");
    }

    private IEnumerator WaitForInput(Player winningPlayer)
    {
        while (pauseGameInput == true)
        {
            if (Input.anyKeyDown)
            {
                pauseGameInput = true;
            }
        }
        yield return new WaitForEndOfFrame();

        SetCurrentCardForEachPlayer(winningPlayer);
        StartNewTurn();
    }

    private void CompareFirstCategory(Character[] cardsToTest)
    {
        Character winningCard = new Character("", "", 0, 0, "", "");
        Character[] cardsToAdd = new Character[numberOfPlayers-1];

        foreach (Character card in cardsToTest)
        {
            int i = 0;

            if (card.NumberOfGames > winningCard.NumberOfGames)
            {
                /*if (i > 0)
                {
                    i = 0;
                    winningCard = null;
                }*/

                winningCard = card;
            }
            /*if (card.NumberOfGames == winningCard.NumberOfGames)
            {
                i++;
                winningCard = card;
            }*/
        }

        foreach (Character card in cardsToTest)
        {
            int i = 0;

            if (card != winningCard)
            {
                cardsToAdd[i] = card;
                i++;
            }
        }

        ChooseWinningCard(winningCard, cardsToAdd);

    }

    private void CompareSecondCategory()
    {

    }

    private void CompareThirdCategory()
    {
        //System.DateTime, parse the string as this then compare
    }

    private void CompareFourthCategory()
    {
        //System.DateTime, parse the string as this then compare
    }

    private void CreatePlayers()
    {
        players = new Queue<Player>();
        for (int i = 0; i < numberOfPlayers; i++)
        {
            Player player = new Player(i);
            if (i == 0)
            {
                player.currentPicker = true;
            }
            players.Enqueue(player);

            player.cardModel.GetComponent<Camera>().rect = new Rect(1 / 1 / (float)numberOfPlayers * i, 0f, 1 / 1 / (float)numberOfPlayers, 1f);
        }
    }

    private void DealCards()
    {
        Dealer dealer = new Dealer(characters);

        while (dealer.HasCardsLeft())
        {
            Player player = players.Dequeue();
            player.AddCard(dealer.GetNextCard());
            players.Enqueue(player);
        }
    }

    public void EvaluateCardFromPlayer(Categories category)
    {
        Character[] cardsToTest = GetAllCurrentCards();

        switch (category)
        {
            case Categories.GAMES:
                CompareFirstCategory(cardsToTest);
                break;
        }
    }

    private Player FindOwnerOfWinningCard(Character winningCard)
    {
        Player winningPlayer = null;
        int i = 0;

        foreach (Player player in players)
        {
            if (player.currentCard == winningCard)
            {
                winningPlayer = player;
                i++;
            }
        }

        return winningPlayer;
    }

    private Character[] GetAllCurrentCards()
    {
        Character[] currentCards = new Character[numberOfPlayers];
        int i = 0;

        foreach (Player player in players)
        {
            currentCards[i] = player.currentCard;
            i++;
        }
        return currentCards;
    }

    private void HideAllCards()
    {
        foreach (Player player in players)
        {
            player.HideCard();
        }
    }

    private void SetCurrentCardForEachPlayer(Player winningPlayer)
    {
        foreach(Player player in players)
        {
            if (player == winningPlayer)
            {
                player.SetCurrentCard(true);
            }
            else
            {
                player.SetCurrentCard(false);
            }
        }
    }

    public void ShowAllCurrentCards()
    {
        foreach (Player player in players)
        {
            player.DisplayCard();
            Debug.Log("Player " + player.playerID + " has " + player.currentCard.CharacterName);
        }
    }

    private void ShowCardOfCurrentPicker()
    {
        foreach (Player player in players)
        {
            if (player.currentPicker == true)
            {
                player.DisplayCard();
            }
        }
    }

    private void ShowNumberOfCardsLeft()
    {
        foreach (Player player in players)
        {
            Debug.Log("Player " + player.playerID + " has " + player.numberOfCardsLeft + " left.");
        }
    }

    private void StartNewTurn()
    {
        UpdatePlayersCardsLeft();
        ShowNumberOfCardsLeft();
        ShowAllCurrentCards();
        HideAllCards();
        ShowCardOfCurrentPicker();
    }

    private void UpdatePlayersCardsLeft()
    {
        foreach (Player player in players)
        {
            player.UpdateCardsLeft();
        }
    }
}

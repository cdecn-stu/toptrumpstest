﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dealer
{
    private Queue<Character> shuffledDeck;

    public Dealer(Character[] cards)
    {
        cards = ShuffleCards(cards);
        this.shuffledDeck = new Queue<Character>(cards);
    }

    public Character GetNextCard()
    {
        if (shuffledDeck.Count > 0) return shuffledDeck.Dequeue();
        else return null;
    }

    public bool HasCardsLeft()
    {
        if (shuffledDeck.Count > 0) return true;
        else return false;
    }

    private Character[] ShuffleCards(Character[] cards)
    {
        for(int i = cards.Length - 1; i > 0; i--)
        {
            // Randomize a number between 0 and i (so that the range decreases each time)
            int rnd = UnityEngine.Random.Range(0, i);

            // Save the value of the current i, otherwise it'll overwrite when we swap the values
            Character temp = cards[i];

            // Swap the new and old values
            cards[i] = cards[rnd];
            cards[rnd] = temp;
        }

        return cards;

        /*
        int placeInDeck;

        for(int i = 0; i < cards.Length; i++)
        {
            do
            {
                placeInDeck = Random.Range(0, cards.Length - 1);
                Debug.Log(placeInDeck);
            } while (shuffledDeck[placeInDeck] != null);

            shuffledDeck[placeInDeck] = cards[i];
        }

        return shuffledDeck;
        */
    }
}

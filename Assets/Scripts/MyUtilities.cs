using UnityEngine;
using System.Xml;
using System.IO;
using System.Collections.Generic;


public class MyUtilities
{    
    public static Character[] LoadXMLData(string filename)
    {
		List<Character> cards = new List<Character>();

		string characterName = "";
		string imageName = "";
		int numberOfGames = 0;
		int numberOfSpinOffSeries = 0;
		string firstAppearance = "";
		string latestAppearance = "";

		TextAsset xmlData = Resources.Load(filename) as TextAsset;
        string xmlString = xmlData.text;

        Character character = null;

        // Create an XmlReader
        using (XmlReader reader = XmlReader.Create(new StringReader(xmlString)))
        {
            while (reader.Read())
            {

                if (reader.LocalName == "character" && reader.IsStartElement())
                {
                    character = null;
                }
                if (reader.IsStartElement())
                {
                    switch (reader.LocalName)
                    {
                        case "name":
                            characterName = reader.ReadElementContentAsString();
                            break;
                        case "image":
                            imageName = reader.ReadElementContentAsString();
                            break;
                        case "games":
                            numberOfGames = reader.ReadElementContentAsInt();
                            break;
                        case "spinoffs":
                            numberOfSpinOffSeries = reader.ReadElementContentAsInt();
                            break;
                        case "first":
                            firstAppearance = reader.ReadElementContentAsString();
                            break;
                        case "latest":
                            latestAppearance = reader.ReadElementContentAsString();
                            
                            break;
                    }
                }
				if (reader.LocalName == "character" && !reader.IsStartElement())
				{
					character = new Character(characterName,
									imageName,
									numberOfGames,
									numberOfSpinOffSeries,
									firstAppearance,
									latestAppearance);
					cards.Add(character);
				}
			}
        }   

		return cards.ToArray();
	}
}
